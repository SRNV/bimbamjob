function Tondeuse ({ initPosition, read, maxXY }){
    
    initPosition?'':console.error('property initPosition is required');
    read?'':console.error('property read is required');

    const pelouse = {
        maxX: parseFloat(maxXY[0]),
        maxY: parseFloat(maxXY[1]),
    }
    this.read = read;
    this.datasHandler = {
        get:( obj, prop ) => prop in obj ? obj[prop] : 0,
        set:( obj, prop, value ) => {
            //console.log(obj,prop,value)
            if(prop === 'x'){
                value > pelouse.maxX ? value = pelouse.maxX : '';
                value < 0 ? value = 0 : '';
            }
            if(prop === 'y'){
                value > pelouse.maxY ? value = pelouse.maxY : '';
                value < 0 ? value = 0 : '';
            }
            if(prop === 'rotation' && value > 360 || value === 360) {
                    obj[prop] = 360 - value;
                    const newrotation =  obj[prop];
                    this.datas.orientation = this.datasOrientations[newrotation];
                    return true;
            }
            obj[prop] = value;
            return true;
        },
    };

    this.datasOrientations = {
        W:0,
        N:90,
        E:180,
        S:270,
        '0':'W',
        '90':'N',
        '180':'E',
        '270':'S',
        '360':'W',
    };

    this.datas = new Proxy({
        x: parseFloat(initPosition[0]),
        y: parseFloat(initPosition[1]),
        rotation: this.datasOrientations[initPosition[2]],
        orientation: initPosition[2],
        'N':() => this.datas.y++,
        'S': () => this.datas.y--,
        'E': () => this.datas.x++,
        'W': () => this.datas.x--,
    },this.datasHandler);



    this.readInstructions = (directive) => {
        console.log('======== instructions reçues')
        directive ? '':console.error('missing one argument');
        typeof directive === 'string' ? '':console.error('Waiting for a string in argument');

        return new Promise((resolve, reject) => {
            try {
                const datas = directive.trim().split('').filter(d => d.trim()!=='');
                datas.map((letter) => {
                    this[`move${letter}`]();
                });
                resolve(this.datas);
            }catch(e){
                console.error(e)
                reject(e);
            }
        })
    }

    this.moveD = () => {
        this.datas.rotation+=+90;
    }

    this.moveG = () => {
        this.datas.rotation+=-90;
    }

    this.moveA = () => {
        this.datas[this.datas.orientation]();
    }

    this.report = () => {
        return this.readInstructions(this.read);
    }

}

module.exports = {
    Tondeuse
};