const  { Tondeuse }  = require('./tondeuse');
function Tondeuses ({file}){
    typeof file === 'string'?'':console.error('waiting for a string argument for Tondeuses prototype');
    const data = file.split('\n').filter(d=>d.trim()!=='');
    const dataMaxRightCornerXY = data[0].split('');
    const programmation = data.filter((d,id)=>id>0);
    let count = 0;
    let tondeuses = [];
    let actualTondeuse = null;

    programmation.map((p,id)=>{
        if(count === 0){
            count ++;
            let coordonees = p.replace(/\s/gi,'').trim().split('');
            actualTondeuse = new Tondeuse ({
                maxXY: dataMaxRightCornerXY,
                initPosition: [...coordonees],
                read: programmation[id+1]? programmation[id+1] :'',
                }
            );
        }else{
            count = 0;
            actualTondeuse.report().then(value=>{
                console.log(value); 
                console.log(`RAPPORT DE POSITION : cette tondeuse se trouve actuellement au point (${value.x},${value.y},${value.orientation}), sa rotation est de ${value.rotation}°.`)
                console.log(`POSITION FINALE : ${value.x}${value.y} ${value.orientation}`);
             });
        }
    })
} 
module.exports = { Tondeuses };